
#include "new.hpp"
#include <iostream>

#include "eastlu.hpp"

#include <thread>
#include <chrono>

int main()
{
	eastl::vector<int> d;
	eastl::contains(d, 4);
	eastl::push_back_once(d, 3);
	eastl::remove(d, 7);

	eastl::list<int> d2;
	eastl::contains(d2, 4);
	eastl::push_back_once(d2, 3);
	eastl::remove(d2, 7);

	eastl::map<int, int> d3;
	eastl::insert_once(d3, {7,8});
	eastl::insert_once(d3, 1,22);
	eastl::remove(d3, 9);

	eastl::unordered_map<int, int> d4;
	eastl::insert_once(d4, {7,8});
	eastl::insert_once(d4, 1,22);
	eastl::remove(d4, 9);

	size_t count = 0;
	auto f = []()
	{
		std::cout << "do once test\n";
	};
	while(true)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		eastl::do_once(f);

		count++;

		if(count == 10)
		{
			eastl::reset_do_once(f);
		}
	}

	return 0;
}
