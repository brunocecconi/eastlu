/************************************************************************************
*                                                                                   *
*   Copyright (c) 2018 Bruno Cecconi <brunocecc5@gmail.com>                         *
*                                                                                   *
*   This file is part of stlu                            							*
*   License: MIT License                                                            *
*                                                                                   *
*   Permission is hereby granted, free of charge, to any person obtaining           *
*   a copy of this software and associated documentation files (the "Software"),    *
*   to deal in the Software without restriction, including without limitation       *
*   the rights to use, copy, modify, merge, publish, distribute, sublicense,        *
*   and/or sell copies of the Software, and to permit persons to whom the           *
*   Software is furnished to do so, subject to the following conditions:            *
*                                                                                   *
*   The above copyright notice and this permission notice shall be included in      *
*   all copies or substantial portions of the Software.                             *
*                                                                                   *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
*   SOFTWARE.                                                                       *
*                                                                                   *
*************************************************************************************/

#pragma once

#include "EASTL/string.h"
#include "EASTL/algorithm.h"
#include "vector.hpp"

namespace eastl
{

static vector<string> split(const string& target, const string& delimiter)
{
    // Code by: https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string
    vector<string> tokens { 100 };
    string::size_type last_pos = target.find_first_not_of(delimiter, 0);
    string::size_type pos = target.find_first_of(delimiter, last_pos);
    while (string::npos != pos || string::npos != last_pos)
    {
        tokens.push_back(target.substr(last_pos, pos - last_pos));
        last_pos = target.find_first_not_of(delimiter, pos);
        pos = target.find_first_of(delimiter, last_pos);
    }
    return tokens;
}

static void replace_all(string& target, const string& from, const string& to)
{
    // Code by: http://bits.mdminhazulhaque.io/cpp/find-and-replace-all-occurrences-in-cpp-string.html
    for (string::size_type i = 0; (i = target.find(from, i)) != string::npos;)
    {
        target.replace(i, from.length(), to);
        i += to.length();
    }
}
static string get_replace_all(const string& target, const string& from, const string& to)
{
    string s = target;
    replace_all(s, from, to);
    return s;
}

static bool contains(const string& target, const string& compareString)
{
    return target.find(compareString) != string::npos;
} 

static string to_lower(const string& target)
{
    string s = target;
    transform(s.begin(), s.end(), s.begin(),
                    [](unsigned char c)
    {
        return tolower(c);
    });
    return s;
}
static string to_upper(const string& target)
{
    string s = target;
    transform(s.begin(), s.end(), s.begin(),
                    [](unsigned char c)
    {
        return toupper(c);
    });
    return s;
}

}