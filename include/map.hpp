/************************************************************************************
*                                                                                   *
*   Copyright (c) 2018 Bruno Cecconi <brunocecc5@gmail.com>                         *
*                                                                                   *
*   This file is part of stlu                            							*
*   License: MIT License                                                            *
*                                                                                   *
*   Permission is hereby granted, free of charge, to any person obtaining           *
*   a copy of this software and associated documentation files (the "Software"),    *
*   to deal in the Software without restriction, including without limitation       *
*   the rights to use, copy, modify, merge, publish, distribute, sublicense,        *
*   and/or sell copies of the Software, and to permit persons to whom the           *
*   Software is furnished to do so, subject to the following conditions:            *
*                                                                                   *
*   The above copyright notice and this permission notice shall be included in      *
*   all copies or substantial portions of the Software.                             *
*                                                                                   *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
*   SOFTWARE.                                                                       *
*                                                                                   *
*************************************************************************************/

#pragma once

#include "EASTL/map.h"

namespace eastl
{

template < typename K, typename V > inline void insert_once(map<K,V>& m,
    const pair<K,V>& p)
{
    if(m.find(p.first) != m.end())
    {
        m.insert(p);
    }
}

template < typename K, typename V > inline void insert_once(map<K,V>& m,
    const K& key, const V& value)
{
    insert_once(m, pair<K, V> {key, value});
}

template < typename K, typename V > inline void remove(map<K,V>& m, 
    const K& value)
{
    auto it = m.find(value);
    if(it != m.end())
    {
        m.erase(it);
    }
}

}