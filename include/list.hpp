/************************************************************************************
*                                                                                   *
*   Copyright (c) 2018 Bruno Cecconi <brunocecc5@gmail.com>                         *
*                                                                                   *
*   This file is part of stlu                            							*
*   License: MIT License                                                            *
*                                                                                   *
*   Permission is hereby granted, free of charge, to any person obtaining           *
*   a copy of this software and associated documentation files (the "Software"),    *
*   to deal in the Software without restriction, including without limitation       *
*   the rights to use, copy, modify, merge, publish, distribute, sublicense,        *
*   and/or sell copies of the Software, and to permit persons to whom the           *
*   Software is furnished to do so, subject to the following conditions:            *
*                                                                                   *
*   The above copyright notice and this permission notice shall be included in      *
*   all copies or substantial portions of the Software.                             *
*                                                                                   *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
*   SOFTWARE.                                                                       *
*                                                                                   *
*************************************************************************************/

#pragma once

#include "EASTL/algorithm.h"
#include "EASTL/list.h"

namespace eastl
{

template < typename T > inline bool contains(const T& vec, 
    const typename T::value_type& value)
{
    return find(vec.begin(), vec.end(), value) != vec.end();
}

template < typename T > inline void push_back_once(T& vec, 
    const typename T::T::value_type& value)
{
    if(!contains(vec, value))
    {
        vec.push_back(value);
    }
}

template < typename T > inline void remove(list<T>& vec, 
    const typename list<T>::value_type& value)
{
    auto it = find(vec.begin(), vec.end(), value);
    if(it != vec.end())
    {
        vec.erase(it);
    }
}

}