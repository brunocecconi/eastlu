/************************************************************************************
*                                                                                   *
*   Copyright (c) 2018 Bruno Cecconi <brunocecc5@gmail.com>                         *
*                                                                                   *
*   This file is part of stlu                            							*
*   License: MIT License                                                            *
*                                                                                   *
*   Permission is hereby granted, free of charge, to any person obtaining           *
*   a copy of this software and associated documentation files (the "Software"),    *
*   to deal in the Software without restriction, including without limitation       *
*   the rights to use, copy, modify, merge, publish, distribute, sublicense,        *
*   and/or sell copies of the Software, and to permit persons to whom the           *
*   Software is furnished to do so, subject to the following conditions:            *
*                                                                                   *
*   The above copyright notice and this permission notice shall be included in      *
*   all copies or substantial portions of the Software.                             *
*                                                                                   *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
*   SOFTWARE.                                                                       *
*                                                                                   *
*************************************************************************************/

#pragma once

#include <stdio.h>
#include <cassert>
#include "string.hpp"

#undef assert

// Default assert, run in all build types.
#define assert(EXP, MSG)            if(!(EXP)) eastl::make_assert(MSG, __FILE__, __LINE__)

// Debug assert, restrict to debug build type
#ifdef NDEBUG 
#define debug_assert(EXP, MSG)      
#else
#define debug_assert(EXP, MSG)      assert(EXP, MSG)
#endif

namespace eastl
{

inline void make_assert(const string& msg, const char* file_name, const int& line)
{
	printf("\nassertion at %s:%d. Message: %s\n", file_name, line, msg.c_str());
	abort();
}

}